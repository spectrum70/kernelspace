/*
 * kernelspace, (C) 2020 - main.js
 */


var canvas = 0;
var context = 0;

class star {
	constructor(width, height, array) {
		this.height = height;
		this.width = width;
		this.array = array;
	}
}

var star_point = new star(1, 1,
	[1, 1, 1, 1]);

var star_small = new star(3, 3,
	[0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0,
	 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0
	]);

var star_medium = new star(5, 5,
	[0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0,
	 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0,
	 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0,
	 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0,
	]);

var star_big = new star(5, 5,
	[0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0,
	 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0,
	 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0,
	 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0,
	]);

function random_pos()
{
	var px = Math.floor(Math.random() * canvas.width);
	var py = Math.floor(Math.random() * canvas.height);

	return [px, py];
}

function get_rand_color()
{
	var color = Math.floor(Math.random() * 3);
	switch (color) {
	case 0:
		return [0, 0xcf, 0xff];
	case 1:
		return [0, 0xd5, 0xf5];
	case 2:
		return [0, 0xb0, 0xff];
	}
}

function draw_star(img, x, y, star)
{
	var line = canvas.width * 4;
	var xp = x * 4;
	var i = 0;

	var color = get_rand_color();

	for (var l = y; l < y + star.height; l++) {
		for (var c = xp; c < xp + (star.width * 4); c += 4) {
			img.data[line * l + c] = star.array[i++] ? color[0] : 0;
			img.data[line * l + c + 1] = star.array[i++] ? color[1] : 0;
			img.data[line * l + c + 2] = star.array[i++] ? color[2] : 0;
			img.data[line * l + c + 3] = star.array[i++] ? 255 : 0;
		}
	}

}
